#!/usr/bin/env python3

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_solve, diplomacy_eval


# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):

    # ----
    # diplomacy_solve
    # ----

    def test_diplomacy_1(self):
        v = diplomacy_solve()
        self.assertEqual(v, None)

    def test_diplomacy_2(self):
        v = diplomacy_solve("A Madrid Hold\nB London Support A\nC Paris Move Venice\nD Amsertdam Support C\nE Austin Support D\nF Dallas Support A\n")
        self.assertEqual(v, "A Madrid\nB London\nC Venice\nD Amsertdam\nE Austin\nF Dallas\n")
        
    def test_diplomacy_3(self):
        v = diplomacy_solve("A Paris Move London\nF London Hold\nD Madrid Support F\nG Venice Move London\nB NewYork Support G\nC Rome Move Madrid")
        self.assertEqual(v, "A [dead]\nB NewYork\nC [dead]\nD [dead]\nF [dead]\nG London\n")

    # ----
    # diplomacy_eval
    # ----

    def test_eval_1(self):
        r = StringIO("A Madrid Hold")
        w = StringIO()
        diplomacy_eval(r, w)
        self.assertEqual(w.getvalue(), "A Madrid\n")

    def test_eval_2(self):
        r = StringIO("A Houston Move Paris\nB Paris Move Houston\nC London Move Paris\n")
        w = StringIO()
        diplomacy_eval(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Houston\nC [dead]\n")

    def test_eval_3(self):
        r = StringIO("  ")
        w = StringIO()
        diplomacy_eval(r, w)
        self.assertEqual(w.getvalue(), "")

    

# ----
# main
# ----

if __name__ == "__main__": #pragma: no cover
    main()

""" #pragma: no cover

$ coverage run --branch TestDiplomacy.py >  TestDiplomacy.out 2>&1


$ cat TestDiplomacy.out
......
----------------------------------------------------------------------
Ran 6 tests in 0.001s

OK


$ coverage report -m                   >> TestDiplomacy.out
......
----------------------------------------------------------------------
Ran 6 tests in 0.001s

OK
Name               Stmts   Miss Branch BrPart  Cover   Missing
--------------------------------------------------------------
Diplomacy.py          74      0     48      0   100%
TestDiplomacy.py      28      0      0      0   100%
--------------------------------------------------------------
TOTAL                102      0     48      0   100%

"""
